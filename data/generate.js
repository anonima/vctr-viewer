import faker from 'faker'
import short_id from 'shortid'
import 'string.prototype.padstart'

/**
 * Generate fake data
 */
export default function create_fake_data({ commit, state }) {
  let book_id = '5a0a6ddbb121b20a59514fdc'
  let url_base = 'https://f.id0.it/vctr-viewer-data/books/' + book_id

  // Get pages
  let pages = Array( 124 ).fill().map((obj, index) => {
    let page_number = index + 1
    let padded_page_number = page_number.toString().padStart(5, 0)
    let image_src =  url_base + '/pages/images/Page_' + padded_page_number + '.jpg'
    let thumbnail_image_src = url_base + '/pages/thumbnails/' + page_number + '.jpg'
    let html_src = url_base + '/pages/html/' + page_number + '.html'

    return {
      _id: 'xxx_' + padded_page_number,
      page_number,
      image_src,
      image_width: 856,
      image_height: 1181,
      thumbnail_image_src,
      thumbnail_image_width: 144,
      thumbnail_image_height: 199,
      html_src
    }
  })

  // Create book
  let author = "А.В. Бабич, А.Л. Манаков, С.В. Щелоков"
  let title = "Ремонт машин в строительстве и на железнодорожном транспорте"

  let book = {
    _id: book_id,
    author,
    title,
    bibliography: author + " " + title,
    pages,
    symbols: 200000
  }

  commit('set_data', {
    key: 'book',
    data: book
  })

  // Create limits
  let limits = {
    printed_pages: 10,
    copied_symbols: 4000,
    printed_pages_allowed: 0.1
  }

  commit('set_data', {
    key: 'limits',
    data: limits
  })

  // Texts
  let notes = Array( 5 ).fill().map((obj, index) => {
    let number = index + 1

    return {
      _id: short_id.generate(),
      title: faker.lorem.words(),
      text: faker.lorem.paragraph(),
      date: faker.date.past()
    }
  })

  commit('set_data', {
    key: 'notes',
    data: notes
  })
}