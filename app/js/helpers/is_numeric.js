/**
 * Check if value is numeric
 * @param {*} element
 * @returns {Boolean}
 */
export default function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}