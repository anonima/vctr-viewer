/**
 * Sort array
 * Pure alternative:
 * `return array.sort((a, b) => b[param] - a[param]);`
 * @param {Array} array
 * @param {String} param
 * @param {String} [order="desc"]
 * @example
 * f_sort([{name: "b", rank: 2}, {name: "x"}, {name: "a", rank: 1}], "rank") // Returns: [{name: "a", rank: 1}, {name: "b", rank: 2}, {name: "x"}]
 * @returns {Array|Boolean} Returns sorted array or false
 */
export default function sort_by_key({array, param, order = "desc"}) {
  // Make checks
  if (!Array.isArray(array) || typeof param === "undefined") {
    return false;
  }
  if (array.length < 1) {
    return array;
  }

  return array.sort((a, b) => {
    // Polyfill for IE 11
    Number.isFinite = Number.isFinite || function(value) {
      return typeof value === 'number' && isFinite(value);
    }

    // Process objects without suitable "param"
    if (!Number.isFinite(a[param])) {
      return 1;
    }

    return order === "desc"
      ? b[param] - a[param]
      : a[param] - b[param];
  });
}