import queryString from 'query-string'
import object_value from './object_value'
// `fetch` polyfill for IE 11
// https://github.com/github/fetch
import 'whatwg-fetch'
// `Promise` polyfill for IE 11
// https://github.com/taylorhakes/promise-polyfill
import 'promise-polyfill/src/polyfill'

/**
 * fetch() a request
 * @param method
 * @param address
 * @param body
 * @param headers
 * @param content_type
 * @param action
 * @param action_on_error
 */
export default function fetch_class({address, method = 'GET', body, headers = {}, content_type, action, action_on_error}) {
  let options = {
    method,
    headers
  }

  // Send credentials (cookies)
  let fetch_credentials = object_value(window, 'viewer_variables.api.fetch_credentials', true)

  if (fetch_credentials === true) {
    options.credentials = 'include'
    options.headers['Access-Control-Allow-Origin'] = '*'
  }

  if (typeof body !== "undefined") {
    options.body = body
  }

  // Add support for `x-www-form-urlencoded` content type
  if (content_type === 'x-www-form-urlencoded') {
    options.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'
    options.body = queryString.stringify(body)
  }

  // Send request
  fetch(address, options)

    // Process response status
    .then(response => {
      if(response.ok) {
        return response.json()
      }
      throw new Error('Network response was not ok.')
    })

    // Process response data
    .then(data => {
      console.log(address, 'data: ', data)

      if (typeof action === "function") {
        action(data)
      }
    })

    // Process response error
    .catch(error => {
      console.log(address, 'error:', error)

      if (typeof action_on_error === "function") {
        action_on_error()
      }
    })
}

export function fetch_post({address, body, action, action_on_error}) {
  fetch_class({
    address,
    method: 'POST',
    content_type: 'x-www-form-urlencoded',
    body,
    action,
    action_on_error
  })
}