import throttle from 'throttle-debounce/throttle'
import debounce from 'throttle-debounce/debounce'
import is_in_view from '../helpers/is_in_view'
import object_value from '../helpers/object_value'

export default {
  props: {
    // page_number: {
    //   type: Number,
    //   required: false
    // },
    page: {
      type: Object,
      required: true
    }
  },
  data() {
    return {
      name: false,
      viewed: false,
      prevent_scroll_to_element: false,
      height: 0
    }
  },
  mounted() {
    this.set_viewed_on_scroll()

    if (this.is_current) {
      this.scroll(0)
    }

    // Get height
    // window.onresize = this.set_height()
  },
  updated() {
    if (this.is_current && !this.prevent_scroll_to_element) {
      this.scroll()
    }
  },
  watch: {
    running_status: {
      handler: function (val, oldVal) {
        // console.log("watch: running_status '" + this.name + "' #" + this.page_number + " - val, oldVal:", val, oldVal)
        if (!val) {
          this.handle_scroll()
        }
      },
      immediate: false
    }
  },
  computed: {
    height_coefficient(source = 'image') {
      let width = this.name === 'thumbnails'
        ? object_value(this, 'page.thumbnail_image_width')
        : object_value(this, 'page.image_width')

      let height = this.name === 'thumbnails'
        ? object_value(this, 'page.thumbnail_image_height')
        : object_value(this, 'page.image_height')

      if (!width || !height) {
        return 297 / 210 // A4 paper size
      }

      return height / width
    },
    page_number() {
      return this.page.page_number
    },
    is_current() {
      return this.$store.state.current_page_number === this.page_number
    },
    running_status() {
      return this.$store.state.running_status[ "scrolling_" + this.name ]
    }
  },
  methods: {
    set_current_page_number() {
      throttle(100, console.log("[mixin__viewer__pages__item] set_current_page_number - this.page_number:", this.page_number))
      this.$store.commit('set_current_page_number', this.page_number)
    },
    set_height() {
      this.height = Math.round( this.$el.offsetWidth * this.height_coefficient )
    },
    scroll: throttle(100, function(speed = 200) {
      const instance = this
      const position_to = instance.$el.offsetTop

      if ( typeof position_to === "undefined" ) {
        return false
      }

      $( instance.$el.offsetParent ).animate({ scrollTop: position_to }, speed)
    }),
    set_viewed_on_scroll() {
      setTimeout(() => {
        if ( !this.viewed
          && is_in_view(this.$el, true)
        ) {
          // console.log("set_viewed_on_scroll '" + this.name + "' #" + this.page_number)
          this.viewed = true
        }
      }, 500)
    },
    handle_scroll() {
      this.set_viewed_on_scroll()
    }
  }
}