if (typeof window.viewer_variables === "undefined") {
  console.error("Missed Viewer variables")
}

// Setup Vue
import Vue from 'vue/dist/vue'
import Vuex from 'vuex'
import fetch_class, { fetch_post } from '../helpers/fetch'
import queryString from 'query-string'
import is_numeric from '../helpers/is_numeric'
import object_value from "../helpers/object_value"
import sort_by_key from '../helpers/sort_by_key'

Vue.use(Vuex)

let w_addresses = window.viewer_variables.api.addresses

const store = new Vuex.Store({
  state: {
    /**
     * API addresses
     */
    api: {
      addresses: {
        get_auth_status:           w_addresses.get_auth_status,
        get_book:                  w_addresses.get_book,
        get_user_data:             w_addresses.get_user_data,
        bookmark_create:           w_addresses.bookmark_create,
        bookmark_update:           w_addresses.bookmark_update,
        bookmark_delete:           w_addresses.bookmark_delete,
        note_create:               w_addresses.note_create,
        note_update:               w_addresses.note_update,
        note_delete:               w_addresses.note_delete,
        post_book_print:           w_addresses.post_book_print,
        post_book_search:          w_addresses.post_book_search,
        user_limit_symbols_update: w_addresses.user_limit_symbols_update,
        user_limit_pages_update:   w_addresses.user_limit_pages_update
      }
    },

    /**
     * Loaded data
     */
    book: {
      _id: null,
      author: '',
      title: '',
      bibliography: '',
      pages: [],
      symbols: null,
      loaded: false
    },
    user: {
      bookmarks: false,
      notes: false,
      limits: {
        printed_pages: null,
        printed_pages_allowed: 1,
        copied_symbols: null,
        copied_symbols_allowed: 1
      },
    },
    auth_status: false,

    /**
     * Common data
     */
    bookmark_colors: window.viewer_variables.bookmark_colors,

    /**
     * Temporary variables
     */
    // - Pages
    current_page_number: 1,
    running_status: {
      scrolling_thumbnails: false,
      scrolling_pages: false
    },

    // - Bookmarks
    current_bookmark: null,

    // - Notes
    current_note: null,
    show_notes: false,
    citation_active: false,

    // - Print
    show_print: false,
    link_to_pdf: null,

    // - Search
    searching: false,
    search: [] // Array of pages
  },
  getters: {
    is_authorised: state => state.auth_status === '1',
    book: state => {
      return state.book.loaded ? state.book : false
    },
    pages: state => {
      let array = object_value(state, 'book.pages', [])
      return sort_by_key({array, param: 'page_number', order: 'asc'})
    },
    pages_for_search: (state, getters) => {
      let pages = getters.pages
      let search = state.search

      if (search.length > 0) {
        pages = pages.filter(obj => {
          return search.indexOf(obj.page_number) > -1
        })
      }

      return pages && pages.length > 0
        ? pages
        : []
    },
    page: (state, getters) => (key = 'page_number', value = state.current_page_number) => {
      return getters.pages.find(obj => obj[key] === value)
    },
    bookmarks: state => {
      let array = object_value(state, 'user.bookmarks', [])
      return sort_by_key({array, param: 'page_number', order: 'asc'})
    },
    bookmark: (state, getters) => (key = 'page_id', value = getters.page()._id) => {
      console.log("getters - bookmark() - key, value:", key, value)
      return getters.bookmarks.find(obj => obj[key] === value)
    }
  },
  mutations: {
    /**
     * Auth status
     */
    set_auth_status(state, param) {
      state.auth_status = param
    },
    /**
     * Set Vuex store data
     * @param state
     * @param obj
     */
    set_data(state, obj) {
      state[obj.key] = obj.data
    },
    /**
     * Pages
     */
    set_current_page_number(state, page_number) {
      console.log("set_current_page_number - in - page_number:", page_number)

      page_number = parseInt( page_number )

      if (!is_numeric(page_number)) {
        return false
      }

      if (state.book.pages.length < page_number) {
        page_number = state.book.pages.length
      }

      if (page_number < 1) {
        page_number = 1
      }

      console.log("set_current_page_number - out - page_number:", page_number)

      state.current_page_number = page_number

      history.pushState(null, '', '?page=' + page_number)
    },
    set_running_status(state, obj) {
      // console.log("set_running_status - obj:", obj)
      state.running_status[obj.name] = obj.status
    },
    /**
     * Bookmarks
     */
    set_current_bookmark(state, _id) {
      console.log("set_current_bookmark - _id:", _id)
      state.current_bookmark = _id
    },
    create_bookmark(state, data) {
      if (!data) {
        console.log('mutation create_bookmark() missed `data` param')
        return
      }

      state.user.bookmarks.unshift( data )

      state.current_bookmark = data._id
    },
    update_bookmark(state, data) {
      state.user.bookmarks.forEach(function(obj) {
        if (obj._id === data._id) {
          obj: data
        }
      });

      state.current_bookmark = null
    },
    delete_bookmark(state, _id) {
      state.user.bookmarks = state.user.bookmarks.filter(function( obj ) {
        return obj._id !== _id
      })

      state.current_bookmark = null
    },
    /**
     * Notes
     */
    set_show_notes(state, show) {
      if (typeof show === "undefined") {
        show = !state.show_notes
      }

      state.show_notes = show
    },
    set_current_note(state, _id) {
      console.log("set_current_note - _id:", _id)
      state.current_note = _id
    },
    create_note(state, data) {
      if (!data) {
        console.log('mutation create_note() missed `data` param')
        return
      }
      state.user.notes.unshift( data )
      state.current_note = data._id
    },
    update_note(state, data) {
      state.user.notes.forEach(function(obj) {
        if (obj._id === data._id) {
          console.log("update_note", obj)

          obj: data
        }
      });
    },
    delete_note(state, _id) {
      state.user.notes = state.user.notes.filter(function( obj ) {
        return obj._id !== _id
      })

      state.current_note = null
    },
    /**
     * Citation
     */
    set_citation_active(state, param) {
      if (typeof param === "undefined") {
        param = !state.citation_active
      }

      state.citation_active = param
    },
    /**
     * Print
     */
    set_show_print(state, param) {
      if (typeof param === "undefined") {
        param = !state.show_print
      }

      state.show_print = param
    },
    set_link_to_pdf(state, param) {
      if (typeof param === "undefined") {
        param = !state.show_print
      }

      state.link_to_pdf = param
    },
    /**
     * Search
     */
    set_search(state, array) {
      state.search = array
    },
    set_searching(state, status) {
      state.searching = status
    },
    /**
     * Limits
     */
    set_copied_symbols(state, amount) {
      state.user.limits.copied_symbols += amount
    },
    set_limit_pages(state, amount) {
      state.user.limits.printed_pages += amount
    },
  },
  actions: {
    /**
     * Check auth status
     */
    get_auth_status({ commit, state }) {
      fetch_class({
        address: state.api.addresses.get_auth_status,
        action(data) {
          let status = object_value(data, 'status')

          commit('set_auth_status', status)
        }
      })
    },
    /**
     * Load data via AJAX request
     */
    load_data({ commit, state }) {
      // Load book
      fetch_class({
        address: state.api.addresses.get_book,
        action(data) {
          if (typeof data !== "undefined") {
            data.loaded = true
            commit('set_data', {key: 'book', data})

            // Get current page from page address
            let pages_length = data.pages.length

            let route_query = queryString.parse(location.search)
            let page_number = parseInt( route_query.page )

            page_number = ( page_number > 0 && page_number <= pages_length )
              ? page_number
              : 1

            commit('set_current_page_number', page_number)
          }
        }
      })

      // Load user related data
      fetch_class({
        address: state.api.addresses.get_user_data,
        action(data) {
          if (typeof data !== "undefined") {
            commit('set_data', {key: 'user', data})
          }
        }
      })
    },
    /**
     * Pages
     */
    decrement_current_page_number({ commit, state }) {
      if (state.current_page_number > 1) {
        let page_number = state.current_page_number - 1
        commit('set_current_page_number', page_number)
      }
    },
    increment_current_page_number({ commit, state }) {
      if (state.book.pages.length >= (state.current_page_number + 1)) {
        let page_number = state.current_page_number + 1
        commit('set_current_page_number', page_number)
      }
    },
    /**
     * Bookmarks
     */
    create_bookmark({ commit, state }) {
      let address = state.api.addresses.bookmark_create
      let page_number = state.current_page_number
      let page_id = store.getters.page()._id

      fetch_post({address,
        body: {
          page_number,
          page_id
        },
        action(data) {
          let _id = data._id

          if (_id) {
            commit('create_bookmark', {
              _id,
              page_number,
              page_id
            })
          }
        }
      })

    },
    update_bookmark({ commit, state }, obj) {
      let address = state.api.addresses.bookmark_update

      fetch_post({address,
        body: obj,
        action(data) {
          if (data._id) {
            commit('update_bookmark', obj)
          }
        }
      })
    },
    delete_bookmark({ commit, state }, _id) {
      let address = state.api.addresses.bookmark_delete

      fetch_post({address,
        body: {_id},
        action(data) {
          let data_id = data._id

          if (data_id) {
            // TODO: Delete by data_id
            // commit('delete_bookmark', data_id)
            commit('delete_bookmark', _id)
          }
        }
      })
    },
    /**
     * Notes
     */
    create_note({ commit, state }) {
      let address = state.api.addresses.note_create
      let page_number = state.current_page_number

      fetch_post({address,
        body: {page_number},
        action(data) {
          let _id = data._id

          if (_id) {
            commit('set_show_notes', true)
            commit('create_note', {_id})
          }
        }
      })
    },
    update_note({ commit, state }, obj) {
      let address = state.api.addresses.note_update

      fetch_post({address,
        body: obj,
        action(data) {
          if (data._id) {
            commit('update_note', obj)
          }
        }
      })
    },
    delete_note({ commit, state }, _id) {
      let address = state.api.addresses.note_delete

      fetch_post({address,
        body: {_id},
        action(data) {
          let data_id = data._id

          if (data_id) {
            // TODO: Delete by data_id
            // commit('delete_note', data_id)
            commit('delete_note', _id)
          }
        }
      })
    },
    /**
     * Print
     */
    run_print({ commit, state }, {first_page_number, last_page_number, total}) {
      let address = state.api.addresses.post_book_print
      let book_id = state.book._id

      commit('set_link_to_pdf', 'waiting')

      fetch_post({address,
        body: {
          book_id,
          first_page_number,
          last_page_number
        },
        action(data) {
          let link_to_pdf = object_value(data, 'link_to_pdf')

          if (link_to_pdf) {
            commit('set_link_to_pdf', link_to_pdf)
            store.dispatch('update_limit_pages', total)
          }
        },
        action_on_error() {
          commit('set_link_to_pdf', 'error')
        }
      })
    },
    /**
     * Search
     */
    run_search({ commit, state }, search_string) {
      let address = state.api.addresses.post_book_search
      let book_id = state.book._id

      commit('set_searching', true)

      fetch_post({address,
        body: {
          book_id,
          search_string
        },
        action(data) {
          let array = data.pages

          if (array.length > 0) {
            commit('set_search', array)
          }

          commit('set_searching', false)
        },
        action_on_error() {
          commit('set_searching', false)
        }
      })
    },
    /**
     * Limits
     */
    update_limit_symbols({ commit, state }, amount) {
      let address = state.api.addresses.user_limit_symbols_update
      let book_id = state.book._id

      fetch_post({address,
        body: {
          book_id,
          amount
        },
        action(data) {
          if (data.status === "ok") {
            commit('set_copied_symbols', amount)
          }
        }
      })
    },
    update_limit_pages({ commit, state }, amount) {
      let address = state.api.addresses.user_limit_pages_update
      let book_id = state.book._id

      fetch_post({address,
        body: {
          book_id,
          amount
        },
        action(data) {
          if (data.status === "ok") {
            commit('set_limit_pages', amount)
          }
        }
      })
    }
  }
})

Vue.directive('focus', {
  inserted: function (el) {
    el.focus()
  }
})

import viewer__auth from './viewer__auth.vue'

const vue_viewer = new Vue({
  el: '.js-viewer',
  store,
  template: '<viewer__auth />',
  components: { viewer__auth }
})