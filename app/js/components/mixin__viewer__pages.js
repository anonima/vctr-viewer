import throttle from 'throttle-debounce/throttle'
import debounce from 'throttle-debounce/debounce'

export default {
  data() {
    return {
      name: false,
      scroll_timeout: null
    }
  },
  mounted() {
    if (this.is_current) {
      throttle(200, this.scroll)
    }

    const $scrolling_element = this.$el.firstChild
    $scrolling_element.addEventListener('scroll', this.handle_scroll)
  },
  destroyed() {
    const $scrolling_element = this.$el.firstChild
    $scrolling_element.removeEventListener('scroll', this.handle_scroll)
  },
  computed: {
    running_status() {
      return this.$store.state.running_status[ "scrolling_" + this.name ]
    }
  },
  methods: {
    set_running_status(status) {
      const name = "scrolling_" + this.name
      this.$store.commit('set_running_status', { name, status })

      clearTimeout(this.scroll_timeout)
      this.scroll_timeout = setTimeout(() => {
        this.$store.commit('set_running_status', { name, status: false })
      }, 200)
    },
    handle_scroll() {
      this.set_running_status(true)
    }
  }
}